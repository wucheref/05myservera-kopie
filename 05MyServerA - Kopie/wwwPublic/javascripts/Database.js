"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Database = void 0;
const Product_1 = require("./Product");
class Database {
    constructor() {
    }
    initProducts() {
        Database.collProducts = new Array;
        try {
            Database.collProducts.push(new Product_1.Product("Notebook", "tool", "black"));
            Database.collProducts.push(new Product_1.Product("Monitor", "tool", "grey"));
            Database.collProducts.push(new Product_1.Product("Borgward", "vehicle", "red"));
            Database.collProducts.push(new Product_1.Product("Bike", "vehicle", "black"));
            Database.collProducts.push(new Product_1.Product("Ski", "tool", "red"));
            Database.collProducts.push(new Product_1.Product("Socks", "other", "red"));
            console;
        }
        catch (error) {
            console.log("==== unexpected error happend: " + error.message + "(" + error.name + ")");
        }
    }
    getAllProducts() {
        return Database.collProducts;
    }
    getFilteredProducts(cat, color) {
        let collFilteredProducts = Database.collProducts;
        if (cat != undefined) {
            collFilteredProducts = collFilteredProducts.filter(product => product.getCategory() == cat);
        }
        if (color != undefined) {
            collFilteredProducts = collFilteredProducts.filter(product => product.getColor() == color);
        }
        return collFilteredProducts;
    }
    FilterById(id) {
        if (id != null) {
            let retProduct = Database.collProducts.find(x => x.getId() == id);
            return retProduct;
        }
    }
    FilterByCategory(cat) {
        let retProduct = Database.collProducts.find(x => x.getCategory() == cat);
        return retProduct;
    }
    addProduct(p) {
        let retProduct = new Product_1.Product(p.getName(), p.getCategory(), p.getColor());
        Database.collProducts.push(retProduct);
        return retProduct;
    }
    updateProductById(id, p) {
        const index = Database.collProducts.findIndex((product) => product.getId() === p.getId());
        if (index < 0) {
            throw new Error(`Product with ID ${p.getId()} not found`);
        }
        Database.collProducts[index].setName(p.getName());
        Database.collProducts[index].setCategory(p.getCategory());
        Database.collProducts[index].setColor(p.getColor());
        return Database.collProducts[index];
    }
    deleteProductById(id) {
        let product = null;
        const index = Database.collProducts.indexOf(product);
        Database.collProducts.splice(index, 1);
        return product;
    }
}
exports.Database = Database;
//# sourceMappingURL=Database.js.map