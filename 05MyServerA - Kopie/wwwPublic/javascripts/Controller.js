"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
const AppError_1 = require("./AppError");
const Database_1 = require("./Database");
const Product_1 = require("./Product");
/**
 * here could be eg. Car/Book-objects
 * as we deal with static pages, ther is no need for that
 */
class Controller {
    constructor() {
        Controller.db = new Database_1.Database();
    }
    initProducts(req, res) {
        Controller.db.initProducts();
        console.log("========= init done =========");
        res.json("init done");
    }
    getAllProducts(req, res) {
        let collFiltetedProducts = Controller.db.getFilteredProducts(req.query.category, req.query.color);
        console.log("===== get all or filtered products");
        res.json(collFiltetedProducts);
    }
    getProductById(req, res, next) {
        let prod;
        try {
            prod = Controller.db.FilterById(req.params.id);
            console.log("=== product by id");
            res.json(prod);
        }
        catch (error) {
            let err = new AppError_1.AppError(error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
    }
    getProductByCategory(req, res) {
        let prod = Controller.db.FilterByCategory(req.params.cat);
        console.log("=== product by category");
        res.json(prod);
    }
    showErrorPage(req, res) {
        console.log("========error page..." + req.originalUrl);
        res.status(404).json({
            message: "Cant find " + req.originalUrl + " on this server",
            status: "fail"
        });
    }
    doErrorHandling(err, req, res, next) {
        console.log("========error handling..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 500;
        err.status = err.status || "unknown error";
        res.status(err.statusCode).json(err);
    }
    addProduct(req, res, next) {
        try {
            console.log("add product" + req.body);
            let newProduct = Product_1.Product.fromJson(req.body);
            newProduct = Controller.db.addProduct(newProduct);
            res.json(newProduct);
        }
        catch (error) {
            let err = new AppError_1.AppError(error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
    }
    deleteProductById(req, res, next) {
        try {
            console.log("deleted product " + req.body);
            let upProd = Product_1.Product.fromJson(req.body);
            Controller.db.deleteProductById(req.params.id);
            res.json(upProd);
        }
        catch (error) {
            let err = new AppError_1.AppError(error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
    }
    updateProductById(req, res, next) {
        try {
            console.log("updated product " + req.body);
            let upProd = Product_1.Product.fromJson(req.body);
            upProd = Controller.db.updateProductById(req.params.id, upProd);
            res.json(upProd);
        }
        catch (error) {
            let err = new AppError_1.AppError(error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
    }
}
exports.Controller = Controller;
//# sourceMappingURL=Controller.js.map