"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
// second line
const express = require("express");
const Controller_1 = require("./Controller");
class App {
    constructor() {
        this.app = express();
        this.controller = new Controller_1.Controller();
    }
    static newInstance() {
        return new App();
    }
    startRouter() {
        //Home page route
        //  this.app.get("/", this.controller.showIndexPage);
        //About page route
        this.app.use(express.json());
        this.app.get("/products", this.controller.getAllProducts);
        this.app.post("/products", this.controller.addProduct);
        this.app.post("/products/init", this.controller.initProducts); //BEFORE /:id otherwise init would be also
        this.app.get("/products/:id", this.controller.getProductById);
        this.app.put("/products/:id", this.controller.updateProductById);
        this.app.delete("/products/:id", this.controller.deleteProductById);
        this.app.all("*", this.controller.showErrorPage); // all other urls send error
        this.app.use(this.controller.doErrorHandling); // register method for general error handling
        this.app.listen(App.port, function () {
            console.log("web server app listening on port " + App.port);
        });
    }
}
exports.App = App;
App.port = 3000;
//# sourceMappingURL=App.js.map
