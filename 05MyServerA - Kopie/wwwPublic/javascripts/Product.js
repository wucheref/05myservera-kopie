"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
class Product {
    constructor(name, categogry, color) {
        Product.lastId++;
        this.id = Product.lastId;
        this.name = name;
        this.category = categogry;
        this.color = color;
    }
    static fromJson(data) {
        let retProduct = new Product(data.name, data.category, data.color);
        retProduct.id = data.id;
        return retProduct;
    }
    setName(name) {
        this.name = name;
        return name;
    }
    setCategory(cat) {
        this.category = cat;
        return cat;
    }
    setColor(color) {
        this.color = color;
        return color;
    }
    getId() {
        return this.id;
    }
    getName() {
        return this.name;
    }
    getCategory() {
        return this.category;
    }
    getColor() {
        return this.color;
    }
    ToString() {
        return "Product: [" + this.id + ", " + this.name + ", " + this.category + ", " + this.color + "]";
    }
}
exports.Product = Product;
Product.lastId = 0;
Product.filterId = -1;
//# sourceMappingURL=Product.js.map