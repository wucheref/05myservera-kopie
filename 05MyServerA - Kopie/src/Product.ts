import { Controller } from "./Controller";

export class Product{
    private static lastId: number =0;
    private static filterId: number = -1;
    private static filterCategory: string;
    private static filtercolor: string;
    private id: number;
    private name: string;
    private category: string;
    private color: string;

    public constructor (name:string, categogry:string, color:string){
        Product.lastId++;
        this.id = Product.lastId;
        this.name = name;
        this.category = categogry;
        this.color = color;
    }

    public static fromJson(data:any):Product{
        let retProduct:Product = new Product(data.name,data.category,data.color);
        retProduct.id = data.id;
        return retProduct;
    }

    public setName(name:string):string{
        this.name= name;
        return name
    }

    
    public setCategory(cat:string):string{
        this.category = cat;
        return cat
    }
    
    public setColor(color:string):string{
        this.color = color
        return color;
    }

    public getId():number{
        return this.id;
    }

    public getName():string{
        return this.name;
    }

    public getCategory():string{
        return this.category;
    }
    public getColor():string{
        return this.color;
    }

    public ToString():string{
        return "Product: [" + this.id + ", " + this.name + ", " + this.category + ", " + this.color + "]";
    }

}