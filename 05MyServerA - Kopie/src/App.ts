import * as express from 'express';
import { Controller } from './Controller';

export class App{
    private static port: number =3000;
    private app: any;   //type will be a function
    private controller: Controller;

    private constructor(){
        this.app = express();
        this.controller = new Controller();
    }

    public static newInstance(){
        return new App();
    }
    
    public startRouter():void{
        //Home page route
      //  this.app.get("/", this.controller.showIndexPage);
        //About page route
        this.app.use(express.json());
        this.app.get("/products", this.controller.getAllProducts);
        this.app.post("/products", this.controller.addProduct);
        this.app.post("/products/init", this.controller.initProducts);       //BEFORE /:id otherwise init would be also
        this.app.get("/products/:id", this.controller.getProductById);
        this.app.put("/products/:id", this.controller.updateProductById);
        this.app.delete("/products/:id", this.controller.deleteProductById);
        this.app.all("*", this.controller.showErrorPage);                   // all other urls send error
        this.app.use(this.controller.doErrorHandling);                      // register method for general error handling
        this.app.listen(App.port, function (){
            console.log("web server app listening on port " + App.port)
        })
        
    }

}