import { FileOptions } from "./FileOptions";
import { AppError } from "./AppError";
import { App } from "./App";
import { Database } from "./Database";
import { Product } from "./Product";
/**
 * here could be eg. Car/Book-objects
 * as we deal with static pages, ther is no need for that
 */

export class Controller{
    private static options:FileOptions;
    private static db:Database;
    public constructor(){
        Controller.db = new Database();
    }
    public initProducts(req, res):void{
        Controller.db.initProducts();
        console.log("========= init done =========")
        res.json("init done");
    }
    public getAllProducts(req, res):void{
        let collFiltetedProducts = Controller.db.getFilteredProducts(req.query.category,req.query.color);
       
        console.log("===== get all or filtered products")
        res.json(collFiltetedProducts);
    }

    

    public getProductById(req, res,next):void{
        let prod:Product;
        try{
            prod= Controller.db.FilterById(req.params.id);
            console.log("=== product by id");
            res.json(prod);
            
        }
        catch(error){
            let err:AppError = new AppError((error as Error).message);
            err.status ="fail";
            err.statusCode = 404;
            next(err);
        }
        
    }

    public getProductByCategory(req, res):void{
        let prod:Product = Controller.db.FilterByCategory(req.params.cat);
        console.log("=== product by category");
        res.json(prod);
        
    }

    public showErrorPage(req, res):void{
        console.log("========error page..." + req.originalUrl);
        res.status(404).json({
            message: "Cant find " + req.originalUrl + " on this server",
            status: "fail"
        });
    }

    
    public doErrorHandling(err: AppError, req, res, next):void{
        console.log("========error handling..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 500;
        err.status = err.status || "unknown error";
        res.status(err.statusCode).json(err);

    }

    public addProduct(req,res,next):void{
        try{
            console.log("add product"+req.body);
            let newProduct:Product = Product.fromJson(req.body);
            newProduct = Controller.db.addProduct(newProduct);
            res.json(newProduct);
        }
        catch(error){
            
                let err:AppError = new AppError((error as Error).message);
                err.status ="fail";
                err.statusCode = 404;
                next(err);
            
        }
    }

    public deleteProductById(req,res,next):void{
        try{
            console.log("deleted product " +req.body);
            let upProd:Product = Product.fromJson(req.body);
            Controller.db.deleteProductById(req.params.id);
            res.json(upProd);
        }
        catch(error){
            
                let err:AppError = new AppError((error as Error).message);
                err.status ="fail";
                err.statusCode = 404;
                next(err);
            
        }
    }
    public updateProductById(req,res,next):void{
        try{
            console.log("updated product " +req.body);
            let upProd:Product = Product.fromJson(req.body);
            upProd = Controller.db.updateProductById(req.params.id,upProd);
            res.json(upProd);
        }
        catch(error){
            
                let err:AppError = new AppError((error as Error).message);
                err.status ="fail";
                err.statusCode = 404;
                next(err);
            
        }
    }

    
}