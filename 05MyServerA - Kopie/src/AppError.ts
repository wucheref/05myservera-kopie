export class AppError{
    public status: string; 
    public statusCode: number;
    public message: string;

    public constructor(message: string){
        this.message = message;
    }
}