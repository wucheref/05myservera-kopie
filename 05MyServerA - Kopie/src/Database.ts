import { Controller } from "./Controller";
import { Product } from "./Product";

export class Database{
    private static collProducts:Array<Product>;
    private  static collFilteredProducts:Array<Product>;
    public constructor(){

    }

    public initProducts(){
        Database.collProducts = new Array<Product>
        try{
            Database.collProducts.push(new Product("Notebook","tool","black"));
            Database.collProducts.push(new Product("Monitor","tool","grey"));
            Database.collProducts.push(new Product("Borgward","vehicle","red"));
            Database.collProducts.push(new Product("Bike","vehicle","black"));
            Database.collProducts.push(new Product("Ski","tool","red"));
            Database.collProducts.push(new Product("Socks","other","red"));
            console
        }

        catch(error){
        console.log("==== unexpected error happend: " + (error as Error).message + "(" + (error as Error).name + ")");
        }

    }
    public getAllProducts():Array<Product>{
        return Database.collProducts;
    }

    public getFilteredProducts(cat:string,color:string) {
      let collFilteredProducts:Array<Product> = Database.collProducts;
      if(cat!=undefined){
        collFilteredProducts = collFilteredProducts.filter(product => product.getCategory() == cat);
      }
      if(color !=undefined){
        collFilteredProducts = collFilteredProducts.filter(product => product.getColor() == color);
      }
      return collFilteredProducts;
      

    }

    public  FilterById(id:number):Product{
        if(id !=null){
            let retProduct:Product= Database.collProducts.find(x=>x.getId() ==id);
            return retProduct;
        }
        
    }

    public  FilterByCategory(cat:string):Product{
        let retProduct:Product= Database.collProducts.find(x=>x.getCategory() ==cat);
        return retProduct;
    }

    public addProduct(p:Product):Product{
        let retProduct:Product = new Product(p.getName(),p.getCategory(),p.getColor());
        Database.collProducts.push(retProduct);
        return retProduct;
    }

    public updateProductById(id:number,p:Product):Product{
        const index = Database.collProducts.findIndex((product) => product.getId() === p.getId());
        if (index < 0) {
             throw new Error(`Product with ID ${p.getId()} not found`);
        }
        Database.collProducts[index].setName(p.getName());
        Database.collProducts[index].setCategory(p.getCategory());
        Database.collProducts[index].setColor(p.getColor());
        return Database.collProducts[index];
    }

    public deleteProductById(id:number):Product{
        let product:Product = null
        const index = Database.collProducts.indexOf(product);
            Database.collProducts.splice(index,1);
        return product;
    }

}